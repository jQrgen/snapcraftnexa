# Use the snapcore/snapcraft image as the base
FROM snapcore/snapcraft

# Update the package list and install software-properties-common
RUN apt-get update && apt-get install -y software-properties-common

# Add the PPA for OpenJDK 17
RUN add-apt-repository ppa:openjdk-r/ppa && apt-get update

# Install git and OpenJDK 17
RUN apt-get install -y git openjdk-17-jdk

# Install curl and CA certificates
RUN apt-get install -y ca-certificates curl

# Ensure CA certificates are updated
RUN update-ca-certificates -f

# Set JAVA_HOME environment variable
ENV JAVA_HOME /usr/lib/jvm/java-17-openjdk-amd64
ENV PATH $JAVA_HOME/bin:$PATH

# Verify Java and CA certificates
RUN java -version
RUN update-ca-certificates -f
